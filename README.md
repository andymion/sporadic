# Sporadic

*Note: this project is very young still, but it now offers a working API. It's
a bit brittle, and subject to revisions, but you can now log in and access private
statuses and read timelines!*

Sporadic is a design experiment for the fediverse. The goal is to port the user interface
of [Diaspora*](https://diasporafoundation.org) so that it works with [Mastodon](https://joinmastodon.org/) and [Pleroma](https://pleroma.social)
.
My intention is to build an interface for long-form status updates that resembles
the older Diaspora UI of the past. There are several reasons for doing this:

1. It demonstrates the versatility of fediverse applications.
2. It demonstrates a world where Diaspora adopts the ActivityPub protocol
3. It helps me learn how to make web clients.

The majority of this project was inspired by an older project by [Kaniini](https://github.com/kaniini) called
[Feather](https://github.com/kaniini/feather), which kind of had some similar goals. I decided to start
over from scratch to get a better sense of how things are supposed to be put together,
and maybe make some early design decisions of my own.

## Goals
* Be compatible with the Mastodon API (as well as some of Pleroma's APIs as well)
* Provide a frontend that's very easy to set up and install
* Visually approximate Diaspora's interface and functionality, but with a
more modern framework and leaner code
* Make it possible for fediverse instances to optionally use this frontend
instead of the default offerings of their respective platforms.
* Make another Pleroma frontend that supports Chat, but isn't itself based on
SoapBox or React
* Once Diaspora's original UI layout is completed, revamp everything into an
actually good UI.


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

### Setting up the client

*Note: these instructions assume you're running a single-tenant instance. The OAuth request is
currently scoped to a user. In the future, this will likely get tweaked for community instances,
but it's currently limited due to how I had to develop things **

In the /static directory, there's a config.json.example file. Copy it over to config.json and
fill out the values.

To get those values, you need to do the OAuth dance. Here's how to do it in cURL:

1. Register your application.

```
curl -XPOST -F client_name=Sporadic -F redirect_uris=urn:ietf:wg:oauth:2.0:oob -F 'scopes=read write follow' https://your.instance/api/v1/apps

```

2. Now, get the OAuth token. Make sure that your grant_type is 'password', and that you supply user credentials!

```
curl -XPOST -F 'client_id=YOUR_CLIENT_ID' -F 'client_secret=YOUR_CLIENT_SECRET' -F 'redirect_uri=urn:ietf:wg:oauth:2.0:oob' -F 'grant_type=password' -F 'username=YOURUSERNAME' -F 'password=YOURPASSWORD' https://your.instance/oauth/token
```

3. Copy and paste those values into the config file, then go ahead and sign in at /login


For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
