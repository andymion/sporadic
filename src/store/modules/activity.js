//  store/modules/activity.js
// Hacked together with very shaky assumptions
// Probably wrong
import axios from 'axios'
const STATUSES_ENDPOINT = 'api/v1/statuses'
var BASEURL

const state = {
  liked: null,
  reshared: null,
  voted: null
}
const getters = {
  likedStatus: state => !!state.like,
  hasReshared: state => state.reshared
}
const actions = {
  async getActivity ({commit}, Activity) {
    await axios({
      method: 'GET',
      url: `${BASEURL}/${STATUSES_ENDPOINT}/` + Activity.id
    })
      .then(resp => {
        this.activity = resp.data
      })
  },
  async getContext ({commit}, Activity) {
    await axios({
      method: 'GET',
      url: `${BASEURL}/${STATUSES_ENDPOINT}/` + Activity.id + '/context'
    })
      .then(resp => {
        this.activity.context = resp.data
      })
  },
  async getFavorites ({commit}, Activity) {
    await axios({
      method: 'GET',
      url: `${BASEURL}/${STATUSES_ENDPOINT}/` + Activity.id + '/favourited_by'
    })
      .then(resp => {
        this.activity.favorites = resp.data
      })
  },
  async getReshares ({commit}, Activity) {
    await axios({
      method: 'GET',
      url: `${BASEURL}/${STATUSES_ENDPOINT}/` + Activity.id + '/reblogged_by'
    })
      .then(resp => {
        this.activity.reshares = resp.data
      })
  },
  async postStatus ({commit}, Activity) {
    await axios({
      method: 'POST',
      url: `${BASEURL}/${STATUSES_ENDPOINT}/`,
      data: Activity
    })
  }
}
const mutations = {
  setLike (state, activity) {
    state.like = activity
  },
  setReshare (state, activity) {
    state.reshared = activity
  }
}
export default {
  configure (cfg) {
    BASEURL = cfg.BASEURL || ''
  },
  state,
  getters,
  actions,
  mutations
}
