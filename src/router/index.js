import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
import Stream from '@/components/timelines/stream'
import Public from '@/components/timelines/public'
import Local from '@/components/timelines/local'
import Login from '@/components/login'
import Profile from '@/components/profile/profile'
import SinglePostView from '@/components/status/single_post'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/stream'
  },
  {
    path: '/stream',
    name: 'Stream',
    component: Stream,
    props: true,
    meta: { requiresAuth: true }
  },
  {
    path: '/public',
    name: 'Public',
    component: Public,
    meta: { guest: true },
    props: true
  },
  {
    path: '/local',
    name: 'Local',
    component: Local,
    meta: { guest: true },
    props: true
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: { guest: true }
  },
  {
    path: '/people/:uid',
    name: 'Profile',
    component: Profile,
    meta: { guest: true },
    props: route => ({ uid: route.params.uid })
  },
  {
    path: '/status/:id',
    component: SinglePostView,
    meta: { guest: true },
    props: route => ({ id: route.params.id })
  }
]

const router = new VueRouter({
  mode: 'history',
  linkExactActiveClass: 'selected',
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (store.getters.isAuthenticated) {
      next()
      return
    }
    next('/login')
  } else {
    next()
  }
})

export default router
