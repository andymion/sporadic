// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueI18n from 'vue-i18n'
import App from './App'
import router from './router'
import store from './store'
import profileAPI from '@/services/api/profile'
import timelineAPI from '@/services/api/timeline'
import activityAPI from '@/services/api/activity'
import listsAPI from '@/services/api/lists'
import Auth from './store/modules/auth'
import Activity from './store/modules/activity'
import axios from 'axios'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueTWEmoji from 'vue-twemoji'
import '@/mixins/formatters'

// Get emoji set up
Vue.use(VueI18n)
Vue.use(VueTWEmoji, {
  extension: '.png',
  size: '100x100'
})

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.config.productionTip = false

const currentLocale = (window.navigator.language || 'en').split('-')[0]
const i18n = new VueI18n({
  locale: currentLocale,
  fallbackLocale: 'en'
})

const token = localStorage.getItem('token')
if (token) {
  axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
}

window.fetch('/static/config.json')
  .then((res) => res.json())
  .then((res) => {
    Auth.configure(res)
    Activity.configure(res)
    activityAPI.configure(res)
    timelineAPI.configure(res)
    profileAPI.configure(res)
    listsAPI.configure(res)

    /* eslint-disable no-new */

    new Vue({
      el: '#app',
      router,
      store,
      i18n,
      components: { App },
      template: '<App/>'
    })
  })
