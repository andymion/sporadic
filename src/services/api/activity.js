import axios from 'axios'
import store from '@/store'
const STATUSES_ENDPOINT = 'api/v1/statuses'
var BASEURL

export default {
  name: 'activityAPI',
  configure (cfg) {
    BASEURL = cfg.BASEURL || ''
  },
  index (params) {
    return axios.get(`${BASEURL}/${STATUSES_ENDPOINT}`, {
      params: params
    })
  },
  likeStatus (id) {
    return axios({
      method: 'POST',
      url: `${BASEURL}/${STATUSES_ENDPOINT}/` + id + '/favourite'
    })
  },

  unlikeStatus (id) {
    return axios({
      method: 'POST',
      url: `${BASEURL}/${STATUSES_ENDPOINT}/` + id + '/unfavourite'
    })
  },

  getStatus (id) {
    return axios({
      method: 'GET',
      url: `${BASEURL}/${STATUSES_ENDPOINT}/` + id
    })
  },

  getContext (id) {
    return axios({
      method: 'GET',
      url: `${BASEURL}/${STATUSES_ENDPOINT}/` + id + '/context'
    })
  },

  getFavorites (id) {
    return axios({
      method: 'GET',
      url: `${BASEURL}/${STATUSES_ENDPOINT}/` + id + '/favourited_by'
    })
  },

  getReshares (id) {
    return axios({
      method: 'GET',
      url: `${BASEURL}/${STATUSES_ENDPOINT}/` + id + '/reblogged_by'
    })
  }
}
